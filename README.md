# all-things-compose-redis-nodejs

This is a simple demo of using Docker Compose project to run a Node.js app with a Redis database.

![App Screenshot](./imgs/home.png)

## Start the Compose Stack

```bash
docker compose up
```

Then open your browser to `http://localhost:7070` to see the app running.

## Do the demo "from scratch"

First, reset the files to the initial state:

```bash
cat > ./compose.yaml <<- EOM
# All things Compose
services:
EOM

rm redis-server.yaml
```

Then follow the steps described in the `docs` folder.
