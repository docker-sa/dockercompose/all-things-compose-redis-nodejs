## Update the compose file

```yaml
services:
  redis-server:
    image: redis:7.2.4
    environment: 
      - REDIS_ARGS="--save 30 1"
    volumes:
      - redis-data:/data
    ports:
      - 6379:6379

volumes:
  redis-data:
    #external: true
```

- Then `docker compose up`
- Then go to Docker Desktop

## Add data to the Redis database
> redis-cli
```bash
SET hamster 12
SET panda 12
SET tiger 68
SET fox 20
```


