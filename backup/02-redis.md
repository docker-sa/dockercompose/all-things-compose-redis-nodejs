```yaml
services:
  redis-server:
    image: redis:7.2.4
    environment: 
      - REDIS_ARGS="--save 30 1"
    volumes:
      - redis-data:/data
    ports:
      - 6379:6379

volumes:
  redis-data:
    #external: true
    
```



```yaml
services:
  redis-server:
    image: redis:7.2.4
    environment: 
      - REDIS_ARGS="--save 30 1"
    volumes:
      - ./data:/data
    ports:
      - 6379:6379
```


## Create a container from the Redis Image

```bash
docker volume create redis-data
docker volume ls

docker run -d -p 6379:6379 \
-e REDIS_ARGS="--save 30 1" \
-v redis-data:/data \
redis:7.2.4
```



